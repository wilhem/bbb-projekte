
#include "gpio.h"

GPIOclass::GPIOclass() 
{
	std::cout << "Constructor of the class  \"GPIO\" called" << std::endl;
}

GPIOclass::~GPIOclass() 
{
	std::cout << "Destructor of the class \"GPIO\" called and executed" << std::endl;

        unexportGPIO();
}
/*
 * 
 * name: unknown
 * @param
 * @return
 * 
 */
int GPIOclass::pinMode( std::string pin_label, unsigned int state ) 
{
	/* Check whether the Pin state has been correctly set or not */
	if( ( state != OUTPUT ) && ( state != INPUT ) ) {
		std::cerr << "Check again the pinMode. It must be OUTPUT or INPUT!" << std::endl;
		return EXIT_FAILURE;
	}

	unsigned int system_pin = calculatePin( pin_label );
        
        /* Store the new pin in the member class array for the destructor */
        used_pins_.push_back( system_pin );
	
	/* Setup the Pin */
	boost::filesystem::fstream fs;
	
	boost::filesystem::path path_file = "/sys/class/gpio/export";
	
	fs.open( path_file, std::fstream::out );
	if( fs.is_open() ) {
		fs << system_pin;
		fs.close();
	} else {
		std::cerr << "Error trying to open \"/sys/class/gpio/export\" file " << std::endl;
	}
	
	boost::filesystem::path path_pin;
	path_pin = "/sys/class/gpio/gpio" + std::to_string( system_pin ) + "/direction";
	
	fs.open( path_pin, std::fstream::out );
	if( fs.is_open() ) {
		if( state ) {
		    fs << "in";
	    } else {
		    fs << "out";
	    }
	    fs.close();
	} else {
		std::cerr << "ERROR: Couldn't open " << path_pin << " file" << std::endl;
	}

	return EXIT_SUCCESS;
}
/*
 * 
 * name: digitalWrite
 * @param 
 * @return 
 * 
 */
int GPIOclass::digitalWrite( std::string pin_label, unsigned int state ) 
{
	/* Check whether the Pin state has been correctly set or not */
	if( ( state != HIGH ) && ( state != LOW ) ) {
		std::cerr << "WARNING: Check again the value you want to write. It must be HIGH or LOW!" << std::endl;
		return EXIT_FAILURE;
	}
	
	unsigned int system_pin = calculatePin( pin_label );
	
	/* Write the desired Pin value */
	boost::filesystem::fstream fs;

	boost::filesystem::path path_pin;
	path_pin = "/sys/class/gpio/gpio" + std::to_string( system_pin ) + "/value";
	
	fs.open( path_pin, std::fstream::out );
	if( fs.is_open() ) {
		fs << state;
	    fs.close();
	} else {
		std::cerr << "ERROR: I couldn't open " << path_pin << " file" << std::endl;
	}

	return EXIT_SUCCESS;
}

unsigned int GPIOclass::digitalRead( std::string pin_label ) 
{
	unsigned int output_value;

	unsigned int system_pin = calculatePin( pin_label );
	
	/* Find the desired Pin value */
	boost::filesystem::ifstream fs;

	boost::filesystem::path path_pin;
	path_pin = "/sys/class/gpio/gpio" + std::to_string( system_pin ) + "/value";

	/* Check whether the Pin has been already generated */
	if( boost::filesystem::exists( path_pin ) ) {
		fs.open( path_pin, std::ios_base::in );
		if( fs.is_open() ) {
			
			std::string str;
		
			std::getline( fs, str );
		
			std::istringstream iss( str );
			iss >> output_value;
        
			return output_value;
			
		} else {
			std::cerr << "ERROR: Couldn't open the file for reading" << std::endl;
		}
        
	} else {
		std::cerr << "WARNING: Run the function \"pinMode()\" before" << std::endl;
		return EXIT_FAILURE;
	}

	return output_value;
}

/**
 *
 * @brief ADC function
 * @return value of the analog measure 
 *
 *
 */
unsigned int GPIOclass::analogRead( std::string pin_label ) 
{
	unsigned int system_pin = calculatePin( pin_label );
        
    /* Enable the analog input driver on the Beaglebone */
	boost::filesystem::ofstream fs;

	boost::filesystem::path path_driver;

	for( size_t i = 0; i < 10; ++i ) {

		path_driver = "/sys/devices/bone_capemgr." + i;

		if( boost::filesystem::is_directory( path_driver ) )  {

				path_driver = path_driver / "slots";

				fs.open( path_driver, std::ios_base::out );

				if( fs.is_open() ) {

				     fs << "BB-ADC";
				     fs.close();

				} else {

				std::cerr << "ERROR: Couldn't open the file for ADC" << std::endl;
				}
		break;
	
		}
	}

	/* NOCH NICHT FERTIG!!!!! */
	system_pin++;
	return 0;

}
 /*
 * 
 * name: calculatePin(
 * @param Chip number and pin number
 * @return It applies the formula for calculating the right pin for the system
 *         GPIO[Chip]_[Pin] => Chip x 32 + Pin 
 * 
 */
unsigned int GPIOclass::calculatePin( std::string pin_label )
{	
	/* Defining local variables */
	char  gpio_pin[5];
	size_t string_length;
	size_t chip_number;
	size_t pin_number;
	char c; // To eat the character
	
	/* Extract the Chip number and the Pin number from the string */
	string_length = pin_label.copy( gpio_pin, ( pin_label.length() - 4 ), 4 );
        gpio_pin[string_length] = '\0';

        std::stringstream str( gpio_pin );
        
        str >> chip_number >> c >> pin_number;
	
	return ( static_cast<unsigned int>( chip_number ) * 32 + static_cast<unsigned int>( pin_number ) );
}

void GPIOclass::unexportGPIO()
{
    /* Unexport all used pins */
    boost::filesystem::fstream fs;

    boost::filesystem::path path_file_unexport = "/sys/class/gpio/unexport";
   
    std::cout << "--Unexporting used pins" << std::endl;
 
    for( size_t i = 0; i < used_pins_.size(); ++i ) {

       fs.open( path_file_unexport, std::fstream::out );
       
       if( fs.is_open() ) {

          fs << used_pins_.at(i);
          fs.close();

       } else {
               std::cerr << "Error trying to open \"/sys/class/gpio/unexport\" file" << std::endl;
       }
    }
}
