#include <iostream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <vector>
#include <fstream>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/filesystem/operations.hpp>

#ifndef _GPIO_H
#define _GPIO_H

#define P8_7  "GPIO2_2"
#define P8_8  "GPIO2_3"
#define P8_9  "GPIO2_5"
#define P8_10 "GPIO2_4"
#define P8_11 "GPIO1_13"
#define P8_12 "GPIO1_12"
#define P8_13 "GPIO0_23"
#define P8_14 "GPIO0_26"
#define P8_15 "GPIO1_15"
#define P8_16 "GPIO1_14"
#define P8_17 "GPIO0_27"
#define P8_18 "GPIO2_1"
#define P8_19 "GPIO0_22"
#define P8_26 "GPIO1_29"

#define P9_11 "GPIO0_30"
#define P9_12 "GPIO1_28"
#define P9_13 "GPIO0_31"
#define P9_14 "GPIO1_18"
#define P9_15 "GPIO1_16"
#define P9_16 "GPIO1_19"
#define P9_17 "GPIO0_5"
#define P9_18 "GPIO0_4"
#define P9_21 "GPIO0_3"
#define P9_22 "GPIO0_2"
#define P9_23 "GPIO1_17"
#define P9_24 "GPIO0_15"
#define P9_25 "GPIO3_21"
#define P9_26 "GPIO0_14"
#define P9_27 "GPIO3_19"
#define P9_41 "GPIO0_20"
#define P9_42 "GPIO0_7"
#define P9_39 "AIN0"
#define P9_40 "AIN1"
#define P9_37 "AIN2"
#define P9_38 "AIN3"
#define P9_35 "AIN6"
#define P9_36 "AIN5"
#define P9_33 "AIN4"

#define HIGH 1
#define LOW 0
#define INPUT 1
#define OUTPUT 0

    class GPIOclass 
    {
        private: 
            /* Variable f�r all die ge�ffnete und benutzte GPIO Pins */
            std::vector<unsigned int> used_pins_;
	
	private:
	    unsigned int calculatePin( std::string pin_label ); 
            void         unexportGPIO();
		
	public:
	       GPIOclass();
	       ~GPIOclass();
		
	       /* Definition der Methode zum Zugriff der GPIO */
               int pinMode( std::string pin_label, unsigned int state );
               int digitalWrite( std::string pin_label, unsigned int state );
               unsigned int digitalRead( std::string pin_label );
               unsigned int analogRead( std::string pin_label );
	};

#endif
