#include "mavlink_control.h"

MavlinkControl::MavlinkControl( int mavlink_ch, const char *serial_name, int baudrate ):
	mavlink_channel_( mavlink_ch ) {
		
	std::cout << "Constructor of the class \"Mavlink Control\" called" << std::endl;
		
	/* Initialisierung der UART-Schnittstelle */
	serial_uart_.init( mavlink_ch, serial_name, baudrate );
	
	/* HOME Verzeichnis wird nun einmal abgespeichert */
	home_path_ = std::string( getenv( "HOME" ) );
	
	waypoint_file_ = "waypoints.txt";
	readWaypointList( waypoint_file_ ); 
	
}

MavlinkControl::~MavlinkControl() {
	
	std::cout << "DESTRUCTOR of the class  \"Mavlink Control\" called" << std::endl;
}

void MavlinkControl::run( void )
{
	/* Hole die empfangenen Nachrichten aus */
	last_messages_ = serial_uart_.getMessage();
	
	display_messages();
}

void MavlinkControl::sendMessage( void )
{
	Mavlink_Item_Waypoint waypoint;
		
	waypoint = waypoint_list_.front();
	
	int frame = waypoint.single_waypoint.frame;
	
	std::cout << frame << " " << std::endl;
	
/*	uint16_t len = mavlink_msg_mission_item_pack( 
					MAVLINK_SYSTEM_ID, 
					MAVLINK_COMP_ID, 
					&tx_message_,
					MAVLINK_TARGET_SYSTEM_ID, 
					MAVLINK_TARGET_COMP_ID, 
					waypoint.single_waypoint.seq,
					frame,
					waypoint.single_waypoint.command, 
					waypoint.single_waypoint.current, 
					waypoint.single_waypoint.autocontinue, 
					waypoint.single_waypoint.param1, 
					waypoint.single_waypoint.param2, 
					waypoint.single_waypoint.param3, 
					waypoint.single_waypoint.param4,
					waypoint.single_waypoint.x, 
					waypoint.single_waypoint.y, 
					waypoint.single_waypoint.z );*/

	uint16_t len = mavlink_msg_mission_item_encode( MAVLINK_SYSTEM_ID, MAVLINK_COMP_ID, &tx_message_,
					&waypoint.single_waypoint );
		
	serial_uart_.writeSerial( tx_message_, len );
					  
}

void MavlinkControl::waypointCountList( void )
{	
	uint16_t len = mavlink_msg_mission_count_pack( MAVLINK_SYSTEM_ID, MAVLINK_COMP_ID, &tx_message_, MAVLINK_TARGET_SYSTEM_ID, MAVLINK_TARGET_COMP_ID, (uint16_t)waypoint_list_.size() );
	
	serial_uart_.writeSerial( tx_message_, len );
}

void MavlinkControl::sendWaypointList( void )
{
	if( serial_uart_.getMissionRequest() ) {
		
		Mavlink_Item_Waypoint waypoint;
		
		waypoint = waypoint_list_.front();

		/* Die Struktur wird direkt in eine Nachricht umgewandelt */
		uint16_t len = mavlink_msg_mission_item_encode( MAVLINK_SYSTEM_ID, MAVLINK_COMP_ID, &tx_message_, &waypoint.single_waypoint );
		
		serial_uart_.writeSerial( tx_message_, len );
	
		waypoint_list_.pop_front();
		
		serial_uart_.setMissionRequest( false );
	}

}

int MavlinkControl::remainingWaypoints( void )
{
	return waypoint_list_.size();
}

uint16_t MavlinkControl::changeRate( void )
{
	uint16_t len = mavlink_msg_request_data_stream_pack( MAVLINK_SYSTEM_ID, MAVLINK_COMP_ID, &tx_message_, MAVLINK_TARGET_SYSTEM_ID, MAVLINK_TARGET_COMP_ID, 3, 20000, 1 );

    return len;
}

void MavlinkControl::clearAllWp( void )
{
	uint16_t len = mavlink_msg_mission_clear_all_pack( MAVLINK_SYSTEM_ID, MAVLINK_COMP_ID, &tx_message_, MAVLINK_TARGET_SYSTEM_ID, MAVLINK_TARGET_COMP_ID );

	serial_uart_.writeSerial( tx_message_, len );
}

void MavlinkControl::display_messages( void )
{
	//display_heartbeat();
	//display_status();
	//display_attitude();
	//display_radio_status();
	//display_gps_raw();
	//display_servo_out_raw();
	//display_nav_controller_output();
	//display_rc_channels_raw();
	//display_local_position_ned();
	//display_statustext();
}

void MavlinkControl::readWaypointList( std::string filename_input )
{
	/* Lade die Daten in die Datei */
	path_to_waypoint_list_ = home_path_ + "/" + filename_input;
	
	if( boost::filesystem::exists( path_to_waypoint_list_ ) ) {
		
		std::cout << "Waypoint list found" << std::endl;
		
		/* Datei, in der die Wegpunkte abgespeichert sind */
		std::ifstream file_reading( path_to_waypoint_list_.c_str(), std::ios::in );
		
		if( file_reading ) {
			
			std::string buffer;
			
			while( std::getline( file_reading, buffer, '\n' ) ) {
				
				/* Speichere die Koordinate und Aktionen für jeden Wegpunkt */
				Mavlink_Item_Waypoint waypoint_struct;
				
				std::istringstream istr;
				istr.str( buffer );

				if( buffer.empty() )
					break;
				
				float frame, current, autocontinue, seq, command, param1, param2, param3, param4, param5, param6, param7, action;
				
				istr >> seq >> current >> frame >> command >> param1 >> param2 >> param3 >> param4 >> param5 >> param6 >> param7 >> autocontinue >> action;
				
				waypoint_struct.single_waypoint.param1 = param1;
				waypoint_struct.single_waypoint.param2 = param2;
				waypoint_struct.single_waypoint.param3 = param3;
				waypoint_struct.single_waypoint.param4 = param4;
				waypoint_struct.single_waypoint.x      = param5;
				waypoint_struct.single_waypoint.y      = param6;
				waypoint_struct.single_waypoint.z      = param7;
		
				waypoint_struct.single_waypoint.seq              = (uint16_t)seq;
				waypoint_struct.single_waypoint.command          = (uint16_t)command;
				waypoint_struct.single_waypoint.target_system    = MAVLINK_TARGET_SYSTEM_ID;
				waypoint_struct.single_waypoint.target_component = MAVLINK_TARGET_COMP_ID; 
				waypoint_struct.single_waypoint.frame            = (uint8_t)frame;
				waypoint_struct.single_waypoint.current          = (uint8_t)current;      
				waypoint_struct.single_waypoint.autocontinue     = (uint8_t)autocontinue;
				
				waypoint_struct.action = (uint8_t)action;
 
				waypoint_list_.push_back( waypoint_struct );
			}
		}
		
		file_reading.close();
		
	} else {
		std::cout << "Error: List with Waypoints not found" << std::endl;
	}
}

bool MavlinkControl::missionRequest( void ) 
{
	return serial_uart_.getMissionRequest();
}



void MavlinkControl::display_heartbeat( void )
{
    std::cout << "Type: "            << (int)last_messages_.heartbeat.type 
			  << ", Autopilot: "     << (int)last_messages_.heartbeat.autopilot 
			  << ", Base mode: "     << (int)last_messages_.heartbeat.base_mode 
			  << ", System status: " << (int)last_messages_.heartbeat.system_status 
			  << ", Mavlink ver: "   << (int)last_messages_.heartbeat.mavlink_version 
			  << ", Custom: "        << (int)last_messages_.heartbeat.custom_mode
			  << std::endl;
}

void MavlinkControl::display_status( void )
{
	std::cout << "Onboard control present: "   << (int)last_messages_.sys_status.onboard_control_sensors_present 
	          << ", Onboard control enabled: " << (int)last_messages_.sys_status.onboard_control_sensors_enabled
			  << ", Onboard control health: "  << (int)last_messages_.sys_status.onboard_control_sensors_health
			  << ", Load: "                    << (int)last_messages_.sys_status.load
			  << ", voltage: "                 << (int)last_messages_.sys_status.voltage_battery
			  << ", battery: "                 << (int)last_messages_.sys_status.current_battery
			  << ", drop: "                    << (int)last_messages_.sys_status.drop_rate_comm
			  << ", error: "                   << (int)last_messages_.sys_status.errors_comm
			  << ", battery remaining: "       << (int)last_messages_.sys_status.battery_remaining
			  << std::endl;
}

void MavlinkControl::display_attitude( void )
{
	std::cout << "Roll: "    << (float)last_messages_.attitude.roll 
			  << ", Pitch: " << (float)last_messages_.attitude.pitch
              << ", Yaw: "   << (float)last_messages_.attitude.yaw
              << std::endl;
}

void MavlinkControl::display_radio_status( void )
{
	std::cout << "Rx errors: " << (int)last_messages_.radio_status.rxerrors 
	          << ", fixed: "   << (int)last_messages_.radio_status.fixed
			  << ", rssi: "    << (int)last_messages_.radio_status.rssi
			  << ", remrssi: " << (int)last_messages_.radio_status.remrssi
			  << ", txbuf: "   << (int)last_messages_.radio_status.txbuf
			  << ", noise: "   << (int)last_messages_.radio_status.remnoise
			  << std::endl;
}

void MavlinkControl::display_gps_raw( void )
{
	std::cout << "Lat: "   << (int)last_messages_.gps_raw_data.lat
              << ", Lon: " << (int)last_messages_.gps_raw_data.lon
              << ", Alt: " << (int)last_messages_.gps_raw_data.alt
              << std::endl;
}

void MavlinkControl::display_servo_out_raw( void )
{
	std::cout << "Thr: "    << (int)last_messages_.servo_raw_data.servo1_raw 
	          << ", Ail: "  << (int)last_messages_.servo_raw_data.servo2_raw 
			  << ", Elev: " << (int)last_messages_.servo_raw_data.servo3_raw 
			  << ", Rud: "  << (int)last_messages_.servo_raw_data.servo4_raw 
			  << ", Aux1: " << (int)last_messages_.servo_raw_data.servo5_raw 
			  << ", Aux2: " << (int)last_messages_.servo_raw_data.servo6_raw 
			  << std::endl;
}

void MavlinkControl::display_nav_controller_output( void )
{
	std::cout << "Nav roll: "         << (float)last_messages_.nav_controller_output.nav_roll
	          << ", Nav pitch: "      << (float)last_messages_.nav_controller_output.nav_pitch
			  << ", Alt error: "      << (float)last_messages_.nav_controller_output.alt_error
			  << ", aspd_error: "     << (float)last_messages_.nav_controller_output.aspd_error
			  << ", xtrack: "         << (float)last_messages_.nav_controller_output.xtrack_error
			  << ", Nav bearing: "    << (int)last_messages_.nav_controller_output.nav_bearing
			  << ", Target bearing: " << (int)last_messages_.nav_controller_output.target_bearing
			  << ", Way point: "      << (int)last_messages_.nav_controller_output.wp_dist
			  << std::endl;
}

void MavlinkControl::display_rc_channels_raw( void )
{		
		std::cout << "Ch1: "   << (int)last_messages_.rc_channel_raw.chan1_raw
			      << ", Ch2: " << (int)last_messages_.rc_channel_raw.chan2_raw
                  << ", Ch3: " << (int)last_messages_.rc_channel_raw.chan3_raw
                  << ", Ch4: " << (int)last_messages_.rc_channel_raw.chan4_raw
                  << ", Ch5: " << (int)last_messages_.rc_channel_raw.chan5_raw
                  << ", Ch6: " << (int)last_messages_.rc_channel_raw.chan6_raw
                  << ", Ch7: " << (int)last_messages_.rc_channel_raw.chan7_raw
			      << ", Ch8: " << (int)last_messages_.rc_channel_raw.chan8_raw
                  << std::endl;
}

void MavlinkControl::display_local_position_ned( void )
{
	std::cout << "System boot: " << (int)last_messages_.local_position_ned.time_boot_ms
			  << ", X: "         << (float)last_messages_.local_position_ned.x
			  << ", Y: "         << (float)last_messages_.local_position_ned.y
			  << ", Z: "         << (float)last_messages_.local_position_ned.z
			  << ", Vx: "        << (float)last_messages_.local_position_ned.vx
			  << ", Vy: "        << (float)last_messages_.local_position_ned.vy
			  << ", Vz: "        << (float)last_messages_.local_position_ned.vz
			  << std::endl;
}

void MavlinkControl::display_statustext( void )
{
	std::cout << "Severity: "  << (int)last_messages_.status_text.severity
	          << ", text: "    << last_messages_.status_text.text
			  << std::endl;
}