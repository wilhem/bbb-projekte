#include <string>
#include <stdlib.h>
#include <vector>
#include <list>
#include <serial.h>
#include <iostream>
#include <fstream>
#include <sstream>

#include <boost/smart_ptr.hpp>
#include <boost/filesystem.hpp>

#include <mavlink.h>


#ifndef _MAVLINK_CONTROL_H_
#define _MAVLINK_CONTROL_H_

#define MAVLINK_SYSTEM_ID 0
#define MAVLINK_COMP_ID   0

#define MAVLINK_TARGET_SYSTEM_ID 0
#define MAVLINK_TARGET_COMP_ID 190


struct Mavlink_Item_Waypoint
{
	mavlink_mission_item_t single_waypoint;
	
	/* Aktion, welche am WP durchgeführt werden muss (bspl. Abwurf) */
	uint8_t action;
};


class MavlinkControl {
	
	/* Private member variables */
	private:
		int mavlink_channel_;
		
		SERIALClass serial_uart_;
		
		/* Empfangene Nachricht */
		mavlink_message_t rx_message_;
		mavlink_status_t  rx_msg_status_;
		
		/* Empfangene Nachrichten werden aus der serial-Klasse heraus gelesen */
		Mavlink_Messages last_messages_;
		
		/* Nachricht zum Senden */
		mavlink_message_t tx_message_;
		
		std::string waypoint_file_;
		
		std::list<Mavlink_Item_Waypoint> waypoint_list_;
		
		std::string home_path_;
		boost::filesystem::path path_to_waypoint_list_;

	/* Public member functions */
	public:
		MavlinkControl( int mavlink_ch, const char *serial_name, int baudrate );
		virtual ~MavlinkControl();
		
		void run( void );
		void sendMessage( void );
		
		uint16_t changeRate( void );
		void clearAllWp( void );
		void sendWaypointList( void );
		int remainingWaypoints( void );

		void waypointCountList( void );
		
		bool missionRequest( void );
		
		
	/* Private member functions */
	private:
	
		void display_messages( void );
		
		void display_heartbeat( void );
		void display_status( void );
		void display_attitude( void );
		void display_radio_status( void );
		void display_gps_raw( void );
		void display_servo_out_raw( void );
		void display_nav_controller_output( void );
		void display_rc_channels_raw( void );
		void display_local_position_ned( void );
		void display_statustext( void );
		
		void readWaypointList( std::string filename_input );

};

#endif