#include <iostream>
#include <cstdlib>
#include <list>

#include <mavlink.h>

#include <boost/asio.hpp>
#include <boost/asio/serial_port.hpp>
#include <boost/system/error_code.hpp>
#include <boost/system/system_error.hpp>
#include <boost/bind.hpp>
#include <boost/thread.hpp>


#ifndef _SERIAL_H_
#define _SERIAL_H_

#define RX_LENGTH_BUFFER 256

struct Mavlink_Messages 
{
    int sysid;
    int compid;

    // Heartbeat
	mavlink_heartbeat_t heartbeat;

	// System Status
	mavlink_sys_status_t sys_status;

	// Attitude
	mavlink_attitude_t attitude;

	// Radio Status
	mavlink_radio_status_t radio_status;

	// GPS raw int
	mavlink_gps_raw_int_t gps_raw_data;

	// Servo output int
	mavlink_servo_output_raw_t servo_raw_data;

	// Nav controller output
	mavlink_nav_controller_output_t nav_controller_output;

	// RC channels
	mavlink_rc_channels_raw_t rc_channel_raw;

	// Local position NED
	mavlink_local_position_ned_t local_position_ned;
	
	mavlink_gps_global_origin_t  global_origin;
	
	mavlink_statustext_t status_text;

	/* ACK Nachricht */
	mavlink_mission_ack_t rx_ack_message;
	
	/* Status Nachricht */
	mavlink_status_t  status_msg;
	
	mavlink_mission_request_t mission_request;
};


class SERIALClass 
{
	/* Private member declarations */
	private:
		boost::asio::io_service io_service_;
		boost::shared_ptr<boost::asio::serial_port> port_;
		boost::mutex mutex_;

		char rx_message[RX_LENGTH_BUFFER];

		std::string rx_message_str_;

		int mavlink_channel_;
		
		Mavlink_Messages last_messages_;
		
		/* Empfangene Nachricht */
		mavlink_message_t rx_msg_;
		mavlink_status_t  rx_msg_status_;
		
		bool mission_request_ = false;


	/* Public member functions */
	public:
		SERIALClass();
		SERIALClass( int mavlink_ch, const char *serial_name, int baudrate );
		virtual ~SERIALClass();

		int init( int mavlink_ch, const char *serial_name, int baudrate );

		int writeSerial( const std::string &buf );
		int writeSerial( mavlink_message_t &message, const uint16_t len );
		int writeSerial( const char *buf, const int &size );
		
		Mavlink_Messages getMessage( void );
		
		bool getMissionRequest( void );
		void setMissionRequest( bool value );
		int  evaluateRequest( void );
		
	/* Private member functions */
	private:
		void saveLastMessage( void );

		void evaluateACK( void );

	/* Protected member functions */
	protected:
		virtual void async_read_some_( void );
        virtual void receiveSerial( const boost::system::error_code &err_code, size_t bytes_transferred );
        virtual void receiveSerial( const std::string &data );
		
		virtual void stop( void );

};

#endif