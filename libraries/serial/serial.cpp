#include "serial.h"

/**
 * @brief Konstruktor der Klasse Serial für die serielle Verbindung
 * Es wird erst das Objekt erzeugt
 * @return nichts
 */
SERIALClass::SERIALClass() {
	std::cout << "Constructor of the class \"Serial\" called" << std::endl;
}

/**
 * @brief Konstruktor der Klasse Serial für die serielle Verbindung
 * Es wird das Objekt erzeugt und bereits initialisiert
 * @return nichts
 */
SERIALClass::SERIALClass( int mavlink_ch, const char *serial_name, int baudrate ) {

	std::cout << "Constructor of the class  \"Serial\" called" << std::endl;
	
    init( mavlink_ch, serial_name, baudrate );
}

/**
 * @brief Destruktor der Klasse Serial für die serielle Verbindung
 * @return nichts
 */
SERIALClass::~SERIALClass() {

	std::cout << "DESTRUCTOR of the class  \"Serial\" called" << std::endl;
	
	stop();
}

/**
 * @brief Initialisierung der seriellen Schnittstelle	
 * @param mavlink_ch: Mavlink Kanal (bspl: MAV_COMM_0, MAV_COMM_1, ...)
 * @param serial_name: serielle Schnittstelle (bspl: /dev/ttyO1)
 * @param baudrate:    Baudrate der seriellen Schnittstelle
 * @return 1 wenn die serielle Schittstelle erfolgreich eröffnet werden konnte, -1 falls Probleme aufgetreten sind
 */

int SERIALClass::init( int mavlink_ch, const char *serial_name, int baudrate ) {

	boost::system::error_code err_code;
	
	mavlink_channel_ = mavlink_ch;

	/* Check ob die serielle Schnittstelle bereits belegt ist */
	if( port_ ) {
		std::cout << "Error: serial port already opened!" << std::endl;
		
		return -1;
	}

	port_ = boost::shared_ptr<boost::asio::serial_port>( new boost::asio::serial_port( io_service_ ) );
	port_->open( serial_name, err_code );

	if( err_code ) {
		std::cout << "Error: serial port cannot be opened: " << err_code.message().c_str() << std::endl;

		return -1;
	}

	/* Optionen und Settings für die serielle Schnittstelle */
	port_->set_option( boost::asio::serial_port_base::baud_rate( baudrate ) );
	port_->set_option( boost::asio::serial_port_base::character_size(8) );
	port_->set_option( boost::asio::serial_port_base::stop_bits( boost::asio::serial_port_base::stop_bits::one ) );
	port_->set_option( boost::asio::serial_port_base::parity( boost::asio::serial_port_base::parity::none ) );
	port_->set_option( boost::asio::serial_port_base::flow_control( boost::asio::serial_port_base::flow_control::none ) );

    
    boost::thread t( boost::bind( &boost::asio::io_service::run, &io_service_ ) );

    /* Start the receiving mode */
    async_read_some_();

	/* Return Erfolgreiche Aktivierung der seriellen Schnittstelle */
	return 1;

}

/**
 * @brief Funktion für die Ausgabe über die serielle Schnittstelle
 * @param buf: std::string, die über UART ausgegeben werden soll
 * @return es wird 1 ausgegeben wenn die Übertragung erfolgreich war, 0 wenn keine Daten übersendet wurden, -1 sonst
 */
int SERIALClass::writeSerial( const std::string &buf )
{
	return writeSerial( buf.c_str(), buf.size() );
}

/**
 * @brief Funktion für die Ausgabe über die serielle Schnittstelle
 * @param buf: Zerlegung des Strings in ein const char Array
 * @param size: Größe des Arrays
 * @return es wird die Anzahl der ausgegeben Zeichen gezeigt wenn die Übertragung erfolgreich war, 0 wenn keine Daten übersendet wurden, -1 sonst
 */
int SERIALClass::writeSerial( const char *buf, const int &size )
{
	boost::system::error_code err_code;

	if( !port_ )
		return -1; // Serial port not available

	if( size == 0 )
		return 0;  // Not data to send

	return port_->write_some( boost::asio::buffer( buf, size ), err_code );
}

/**
 * @brief Funktion für die Ausgabe über die serielle Schnittstelle
 * @param message: mavlink Nachricht, die übertragen werden soll
 * @param len: Anzahl der Zeichen, woraus die Nachricht besteht
 * @return es wird die Anzahl der ausgegeben Zeichen gezeigt wenn die Übertragung erfolgreich war, 0 wenn keine Daten übersendet wurden, -1 sonst
 */
int SERIALClass::writeSerial( mavlink_message_t &message, const uint16_t len  )
{
	char buffer[MAVLINK_MAX_PACKET_LEN];
	
	mavlink_msg_to_send_buffer( (uint8_t *)buffer, &message );
	
	return writeSerial( buffer, len );	
}

/**
 * @brief Funktion für das Einlesen aus der seriellen Schnittstelle
 * Es wird geprüft, ob die Schnittstelle bereits belegt ist.
 */
void SERIALClass::async_read_some_( void )
{
	if( port_.get() == NULL || !port_->is_open() )
		return;

	port_->async_read_some( 
			boost::asio::buffer( rx_message, RX_LENGTH_BUFFER ), 
			boost::bind( &SERIALClass::receiveSerial, 
			this, 
			boost::asio::placeholders::error, 
			boost::asio::placeholders::bytes_transferred ) );
	
}

/**
 * @brief Funktion für das Einlesen der Nachrichten über die serielle Schnittstelle
 * Die eingelesene Nachricht (mavlink_message_t) wird in einer Structur abgespeichert (Mavlink_Message) und mit den letzten empfangenen Nachrichten ständig aktualisiert
 * @param err_code
 * @param bytes_transferred
 */
void SERIALClass::receiveSerial( const boost::system::error_code &err_code, size_t bytes_transferred )
{
	boost::mutex::scoped_lock look( mutex_ );
	
	if( port_.get() == NULL || !port_->is_open() ) return;

	if( err_code ) {
		async_read_some_();
		return;
	}

	for( unsigned int i = 0; i < bytes_transferred; ++i ) {
		
		int msg_decode_flag = 0;

		char c = rx_message[i];
		
		msg_decode_flag = mavlink_parse_char( mavlink_channel_, (uint8_t)c, &rx_msg_, &rx_msg_status_ );
		
		/* Wenn eine Nachricht entschlüsselt wird, dann break die for-Schleife */
		if( msg_decode_flag ) {
		
			/* Die empfangene Nachrichten werden abgespeichert */
			saveLastMessage();
		}

	}

	async_read_some_();
}

void SERIALClass::receiveSerial( const std::string &data )
{
	std::cout << "Empfangene Nachricht: " << data << std::endl;
}

Mavlink_Messages SERIALClass::getMessage( void ) 
{
	return last_messages_;
}

void SERIALClass::saveLastMessage( void )
{
	last_messages_.sysid  = rx_msg_.sysid;
	last_messages_.compid = rx_msg_.compid;
 
	switch( rx_msg_.msgid )
	{
		
		case MAVLINK_MSG_ID_HEARTBEAT: // Code: 0
			 mavlink_msg_heartbeat_decode( &rx_msg_, &(last_messages_.heartbeat) );
			 break;

        case MAVLINK_MSG_ID_SYS_STATUS:
			 mavlink_msg_sys_status_decode( &rx_msg_, &(last_messages_.sys_status) );
			 break;

        case MAVLINK_MSG_ID_ATTITUDE:  // Code: 30 
			 mavlink_msg_attitude_decode( &rx_msg_, &(last_messages_.attitude) );
			 break;

        case MAVLINK_MSG_ID_RADIO_STATUS: // Code: 109
			 mavlink_msg_radio_status_decode( &rx_msg_, &(last_messages_.radio_status) );
             break;

        case MAVLINK_MSG_ID_GPS_RAW_INT: // Code: 24
             mavlink_msg_gps_raw_int_decode( &rx_msg_, &(last_messages_.gps_raw_data) );
			 break;

        case MAVLINK_MSG_ID_SERVO_OUTPUT_RAW: // Code: 36
             mavlink_msg_servo_output_raw_decode( &rx_msg_, &(last_messages_.servo_raw_data) );
			 break;

        case MAVLINK_MSG_ID_NAV_CONTROLLER_OUTPUT: // Code: 62
		     mavlink_msg_nav_controller_output_decode( &rx_msg_, &(last_messages_.nav_controller_output) );
			 break;

        case MAVLINK_MSG_ID_RC_CHANNELS_RAW: // Code: 35
			 if( mavlink_msg_rc_channels_raw_get_port( &rx_msg_ ) == 0 ) {
				 mavlink_msg_rc_channels_raw_decode( &rx_msg_, &(last_messages_.rc_channel_raw) );
			 }
			 break;

        case MAVLINK_MSG_ID_LOCAL_POSITION_NED:  // Code: 32
             mavlink_msg_local_position_ned_decode( &rx_msg_, &(last_messages_.local_position_ned) );

			 break;
			 
	    case MAVLINK_MSG_ID_STATUSTEXT: // Code: 253
		     mavlink_msg_statustext_decode( &rx_msg_, &(last_messages_.status_text) );
			 break;

		case MAVLINK_MSG_ID_MISSION_ACK: // Code: 47
			 mavlink_msg_mission_ack_decode( &rx_msg_, &(last_messages_.rx_ack_message) );
			 /* Zeige dass die Nachricht empfangen wurde */
			 evaluateACK();
			 break;
			 
		case MAVLINK_MSG_ID_MISSION_REQUEST: // Code: 40
			 mavlink_msg_mission_request_decode( &rx_msg_, &(last_messages_.mission_request) );
			 mission_request_ = true;
			 break;
		 
		case MAVLINK_MSG_ID_GPS_GLOBAL_ORIGIN: // Code: 49
			 mavlink_msg_gps_global_origin_decode( &rx_msg_, &(last_messages_.global_origin) );
			 break;
			
		default: 
		     std::cout << "Lebt nicht!" << std::endl;
             std::cout << (int)(rx_msg_.msgid) << std::endl;
			break;
	}
	
	last_messages_.status_msg = rx_msg_status_;
}

void SERIALClass::evaluateACK( void )
{
	std::cout << "ACK: " << (int)last_messages_.rx_ack_message.type << " " << std::endl;
}

int SERIALClass::evaluateRequest( void )
{
	return (int)last_messages_.mission_request.seq;
}

bool SERIALClass::getMissionRequest( void )
{
	return mission_request_;
}

void SERIALClass::setMissionRequest( bool value )
{
	mission_request_ = value;
}

void SERIALClass::stop( void )
{
	boost::mutex::scoped_lock look( mutex_ );

	if( port_ ) {

		port_->cancel();
		port_->close();
		port_.reset();
	}

	io_service_.stop();
	io_service_.reset();
}