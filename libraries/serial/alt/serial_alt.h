#include <iostream>
#include <sstream>
#include <iomanip>
#include <stdio.h>    /* Standard Input und Output Funktionen */
#include <stdlib.h>
#include <cstring>
#include <string>
#include <unistd.h>   /* UNIX standard Definition von Funktionen */
#include <fcntl.h>    /* File Control Definition */
#include <errno.h>    /* Error number Definition */
#include <termios.h>  /* POSIX terminal control Definition */
#include <sys/ioctl.h>


#ifndef _SERIAL_H
#define _SERIAL_H

#define RX_LENGTH_BUFFER 256

   class SERIALclass
   {
	   /* Private members declarations */
	   private:
		    struct termios tty_;
		    struct termios tty_old_;         // For saving old settings
		    int    serial_port_id_;
		    const char *serial_port_device_;

		    const speed_t baudrate_;
                   
            unsigned char rx_message[RX_LENGTH_BUFFER];

           /* Public methods declarations */
	   public:
		   SERIALclass( const char *serial_name, const speed_t baud );
           ~SERIALclass();

		   int writeSerial( unsigned char outcoming_message[] );
           int writeSerial( std::string outcoming_message );
		   unsigned char *readSerial();
           unsigned int availableSerial();
		   void closeSerial();
   };

#endif
