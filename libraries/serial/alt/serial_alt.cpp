#include "serial.h"

/***********************************************************************
* Most of this options have set with the informations available at the *
* following link: https://www.cmrr.umn.edu/~strupp/serial.html         *
***********************************************************************/

SERIALclass::SERIALclass( const char *serial_name, const speed_t baud ) : 
	serial_port_device_( serial_name ),
	baudrate_( baud )
    {
        std::cout << "Constructor of the class  \"Serial\" called" << std::endl;

		/* Creation of the object Serial Port */
		serial_port_id_ = open( serial_port_device_, O_RDWR | O_NOCTTY | O_NDELAY );

		memset( &tty_, 0, sizeof( tty_ ) );

		/* Error messages */
		if( tcgetattr( serial_port_id_, &tty_ ) != 0 ) {
			std::cout << "Error " << errno << " from tcgetattr: " << strerror( errno ) << std::endl;
		}
	        
                /* Save the old settings before overwrite them */
	    tty_old_ = tty_;

		/* Baudrate Input and Output */
		cfsetospeed( &tty_, baudrate_ );
		cfsetispeed( &tty_, baudrate_ );

		/* Settings */
        tty_.c_iflag &= ~( IXON | IXOFF | IXANY );   // No software handshake
        tty_.c_oflag = 0;
        tty_.c_lflag &= ~( ICANON | ECHO | ECHOE | ISIG );
		tty_.c_cflag &= ~PARENB;
		tty_.c_cflag &= ~CSTOPB;   // 1 Stopbit
		tty_.c_cflag &= ~CSIZE;
		tty_.c_cflag |= CS8;

		tty_.c_cflag &= ~CRTSCTS;
		/* For a better understanding of the following options see the following link: http://www.unixwiz.net/techtips/termios-vmin-vtime.html */
        tty_.c_cc[VMIN]  = 1;
		tty_.c_cc[VTIME] = 1;
		tty_.c_cflag |= CREAD | CLOCAL;

		/* Flush data */
		tcflush( serial_port_id_, TCIFLUSH );
		if( tcsetattr( serial_port_id_, TCSANOW, &tty_ ) != 0 ) {
			std::cout << "Error " << errno << " from tcsetattr" << std::endl;
		}

	}	

SERIALclass::~SERIALclass()
{
    std::cout << "Destructor of the class \"Serial\" called and executed" << std::endl;

	close( serial_port_id_ );
}

int SERIALclass::writeSerial( unsigned char outcoming_message[] )
{
        unsigned char *tx_buffer;

        int n_written = 0;
        unsigned int element_counter = 0;

        tx_buffer = outcoming_message;

        while( ( *tx_buffer != '\n' ) && ( element_counter < RX_LENGTH_BUFFER ) ) {
            ++element_counter;
            *tx_buffer++;
        }

        n_written = write( serial_port_id_, &outcoming_message[0], element_counter );

        if( n_written < 0 ) {

            std::cout << "Error sending data though the serial port" << std::endl;

            return (-1);
        }

        return 0;
}

int SERIALclass::writeSerial( std::string outcoming_message )
{
    int n_written = 0;

    n_written = write( serial_port_id_, outcoming_message.c_str(), outcoming_message.length() );

    if ( n_written < 0 ) {

        std::cout << "Error sending data through the serial port" << std::endl;

        return (-1);

    }

    return (0);

}

unsigned char *SERIALclass::readSerial()
{
        /* Fill the array with '\0' */
        memset( rx_message, '\0', sizeof( rx_message ) );

        int byte_counter = read( serial_port_id_, (void*)rx_message, RX_LENGTH_BUFFER - 1 );

        if(  byte_counter < 0 ) {

            std::cout << "No bytes received" << std::endl;
        
        } else if ( byte_counter == 0 ) {

            std::cout << "No data waiting" << std::endl;

        } 

        rx_message[byte_counter] = '\n';
        
        std::stringstream oss;
        oss << std::hex << std::setfill('0');

        for ( size_t i = 0; i != byte_counter; ++i ) {

            oss << std::setw(2) << static_cast<unsigned>( rx_message[i] );

        }

        std::string out = oss.str();

        std::cout << out << std::endl;

        return &rx_message[0];
}

unsigned int SERIALclass::availableSerial()
{
    unsigned int bytes = 0;
   
    ioctl( serial_port_id_, FIONREAD, &bytes );
    
    return bytes;
}

void SERIALclass::closeSerial()
{
    close( serial_port_id_ );
}
