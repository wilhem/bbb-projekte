
#include "i2c_connection.h"

/************************************************************************************
*                                                                                   *
* Most of the documentation for working with the i2c bus under Linux                *
* has been taken here: https://www.kernel.org/doc/Documentation/i2c/dev-interface   *
*                                                                                   *
************************************************************************************/

I2Cclass::I2Cclass()
{
    std::cout << "Constructor of the class \"I2C\" called" << std::endl;
}

I2Cclass::~I2Cclass()
{
    std::cout << "Destructor of the class \"I2C\" called and executed" << std::endl;
}

int I2Cclass::i2c_open( unsigned int bus, unsigned int addr )
{
   switch( bus ) {
       case 0:
           std::cout << "The bus " << bus << " is used for HDMI. Sure you want use it?" << std::endl;
           return -1;

       case 1:
           std::cout << "The bus " << bus << " on the BeagleBone Black is not enable as default. Change it." << std::endl;
           return -1;

       case 2:
           std::cout << "Selected i2c Bus: " << bus << std::endl;
           path_i2c_bus_ = "/dev/i2c-" + std::to_string( bus );

           if( ( file_ = open( path_i2c_bus_.c_str(), O_RDWR ) ) < 0 ) {
               printf( "Error opening the i2c-%d bus: %s\n", bus, strerror( errno ) );
               return file_;
           }

           i2c_set_address( addr );

           return 0;

       default:
           return -1;
   }
           
}

int I2Cclass::i2c_set_address( unsigned int address )
{
    /* Once that the device has been opened, the address of the slave device must be already given */
    if( ioctl( file_, I2C_SLAVE, address ) < 0 ) {

        printf( "Error opening the slave address: %s\n", strerror( errno ) );
        return file_;
    }

    return 0;
}

int I2Cclass::i2c_write_byte( unsigned int value )
{
    if( write( file_, &value, 1 ) != 1 ) {

        fprintf( stderr, "i2c_write_byte error: %s\n", strerror( errno ) );
        return -1;

    }

    return 0;
}

int I2Cclass::i2c_read_byte( unsigned int value )
{
    if( read( file_, &value, 1 ) != 1 ) {
        fprintf( stderr, "i2c_read_byte error: %s\n", strerror( errno ) );
        return -1;
    }

    return 0;

}

int I2Cclass::i2c_close()
{
    if( close( file_ ) != 0 ) {
        fprintf( stderr, "i2c_close_error: %s\n", strerror( errno ) );
        return -1;
    }

    return 0;
}
