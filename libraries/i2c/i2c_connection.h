/************************************************
 *  i2c_connection library functions            *
 *                                              *
 *  Davide Picchi                               *
 *  19 Mai 2015 - v 1.0                         *
 ***********************************************/

#include <stdio.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <errno.h>
#include <string>
#include <iostream>
#include <boost/filesystem.hpp>

#ifndef _I2C_H
#define _I2C_H


    class I2Cclass 
        {
            private:
                /* Declaration of private member variables */
                boost::filesystem::path path_i2c_bus_;
                /* Declaration of the opened i2c device */
                int file_;

            public:
                /* Declaration of public member functions/methoden */
                I2Cclass();
                ~I2Cclass();

                int i2c_open( unsigned int bus, unsigned int addr );
                int i2c_set_address( unsigned int addr );

                int i2c_write_byte( unsigned int value );
                int i2c_read_byte( unsigned int value ); 

                int i2c_close();

        };

#endif
