/*
* https://www.guyrutenberg.com/2013/01/27/using-stdchronohigh_resolution_clock-example/
* https://www.daniweb.com/programming/software-development/code/445750/simple-timer-using-c-11s-chrono-library
*/

#include <iostream>
#include <chrono>

#ifndef _TIMER_MAV_
#define _TIMER_MAV_

class Timer 
{
	/* Private member variables */
	private:
		std::chrono::high_resolution_clock::time_point time_start_;

	/* Public member variables */
	public:
		explicit Timer( bool run );
		virtual ~Timer();

		void reset( void );

		std::chrono::milliseconds elapsed( void );

};

#endif