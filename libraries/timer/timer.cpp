#include "timer.h"

/**
* @brief Konstructor
*/
Timer::Timer( bool run = false )
{
	if( run )
	reset();
}

Timer::~Timer() 
{}

void Timer::reset( void )
{
	time_start_ = std::chrono::high_resolution_clock::now();
}

std::chrono::milliseconds Timer::elapsed( void )
{
	return ( std::chrono::duration_cast<std::chrono::milliseconds>( std::chrono::high_resolution_clock::now() - time_start_ ) );
}