#include <iostream>
#include <string>
#include <cstdlib>
#include <unistd.h>
#include <chrono>
#include <ctime>

#include <gpio.h>
#include <serial.h>
#include <mavlink_control.h>
#include <timer.h>

#define pinout P8_18

int main()
{
	int timedelay = 10;

	char toggle = 0;

	/* Warte 0.5 Sekunden, um Daten zu sammeln */
	usleep( 500000 );

	Timer timer_mav( false );

    timer_mav.reset();


	GPIOclass GPIO;

	GPIO.pinMode( pinout, OUTPUT );

	while(1) {

		if( timer_mav.elapsed() > ( std::chrono::milliseconds( timedelay ) ) ) {

			timer_mav.reset();

			if( toggle == 0 ) {

				GPIO.digitalWrite( pinout, HIGH );
			}
			
			if( toggle == 1 ) {

				GPIO.digitalWrite( pinout, LOW );
			}

			toggle == 0 ? toggle = 1 : toggle = 0;
		}
	}
	
	return EXIT_SUCCESS;
}
